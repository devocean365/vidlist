# Vidlist

A simple youtube wishlist protytype bootstrapped with [Create React App](https://github.com/facebook/create-react-app). Not that it makes the world a better place, but it certainly contributes to the wellbeing of my resume :)

## Installation

```sh
git clone git@bitbucket.org:devocean365/vidlist.git
cd vislist
yarn install
yarn start
```

## About

This app allows you to keyword search for youtube videos and save the ones you like for later. It utilizes Youtube V3 API's /search GET service for data fetching and localStorage as wishlist persistence layer. Enjoy!

## Version & TODOs

This is a pre-alpha if you will. Need to write further unit tests and DRY-up the code.

## Possible future features

- custom wishlists with ordering
- autoplay next song from current list
- cloud intergration
- port to react-native