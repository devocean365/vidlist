import React from 'react';

export const VidPlayer = (props) => {
	return <iframe title="video-player" className="frame-responsive" width="900" height="520" src={"https://www.youtube-nocookie.com/embed/" + props.vid + "?rel=0&showinfo=0&autoplay=1"} frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen></iframe>
}