import React, {Component} from 'react';
import {fire, classParent, storageGet, storageSet, storageRemove, getVideoIndexByVideoId} from '../../tools/helpers';
import {VidPlayer} from '../VidPlayer';
import {VID_LIST_STORAGE_KEY} from '../../config';

export class VidList extends Component {

  static contextTypes = {}

  searchTimeout = 1
  state = {
    vidList: [],
  }

  componentDidMount() {
    this.setState({vidList: this.fetchVidList()});
  }

  handlePlayVideo = (e) => {
    e.preventDefault();
    var tgt = classParent(e.target, 'card-action', true);
    fire('showModal', {
      heading: tgt.dataset.title,
      markup: <div className="modal-menu"><VidPlayer vid={tgt.dataset.vid}/></div>, 
      close: true,
      wide: true,
      classes: 'dark tall centered bottom',
    });
  }
  
  fetchVidList() {
    const vidList = storageGet(VID_LIST_STORAGE_KEY);
    if('undefined' === typeof vidList || !vidList) {
      storageSet(VID_LIST_STORAGE_KEY, JSON.stringify([]))
      return [];
    }

    return JSON.parse(vidList);
  }

  resetVidlistStorage() {
    if(!this.state.vidList.length) {
      storageRemove(VID_LIST_STORAGE_KEY);
      
    } else {

      storageSet(VID_LIST_STORAGE_KEY, JSON.stringify(this.state.vidList));
    }


    fire('vidListRefreshed', {vidList: this.state.vidList});
  }

  toggleVidlistVideo = (e) => {
    e.preventDefault();

    const tgt = classParent(e.target, 'card-action', true);
    const videoId = tgt.dataset.vid;
    const vidList = this.state.vidList;

    var indexFound = getVideoIndexByVideoId(videoId, vidList);
    if(indexFound !== false) {
      vidList.splice(indexFound, 1);
      this.setState({ vidList: vidList}, this.resetVidlistStorage);
    }
    
    
  }

  resultItem = (item, key) => {

    return (
      <div key={'item-video' + key} className="card">

        <div className="card-inner clear">

          <span className="card-image">
            <img alt={item.snippet.title} src={item.snippet.thumbnails.medium.url} />
          </span>

          <div className="card-title clear">{item.snippet.title}</div>

          <div className="card-actions">
            <a href="play" title="Play" onClick={this.handlePlayVideo} data-title={item.snippet.title} data-vid={item.id.videoId} className="card-action">
              <i className="icon icon-play icon-l"></i>
            </a>
            <a href="wishlist" title="Save to wishlist" onClick={this.toggleVidlistVideo} data-index={key} data-vid={item.id.videoId} className="card-action">
              <i className={"icon icon-close icon-l"}/>
            </a>
          </div>

        </div>
      </div>
    )

  }

  render() {

    return (
      <div className="search-wrap clearfix">
        <div className="results-wrap clearfix">{this.state.vidList.map(this.resultItem)}</div>
      </div>
    )
  }
}