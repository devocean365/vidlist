import React, {Component} from 'react';

export default class SpinLoadImage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: 0,
      portrait: 0,
      src: null
    }
    this.img = null;
    this.spinner = null;

    this.refreshImg = this._refreshImg.bind(this);
    this.createSetImage = this._createSetImage.bind(this);
    this.onImageError = this._onImageError.bind(this);
    this.isTallPortrait = this._isTallPortrait.bind(this);
    this.onImgLoad = this._onImgLoad.bind(this);
    this.onImgLoadError = this._onImgLoadError.bind(this);

  }

  componentDidMount() {

    if(!this.props.src)
      return;

    this.refreshImg();

  }

  shouldComponentUpdate(nextProps, nextState) {

    if(this.props.src !== nextProps.src)
      this.refreshImg(nextProps.src);

    return true;
      
  }

  _refreshImg() {
    
    this.setState({
      error: 0
    });
    this.createSetImage();

  }

  _onImageError() {
    this.setState({
      error: true
    });
  }

  _imgCached = function(src) {
    var image = new Image();
    image.src = src;

    return image.complete;
  }

  _createSetImage(src) {

    var source = src || this.props.src;

    if(!this.img) {
      this.img = document.createElement('img');
      this.refs.spinLoad.appendChild(this.img);

      if(!this._imgCached(source)) {
        this.spinner = document.createElement('i');
        this.spinner.className = "icon spinner icon-spinner icon-xxl";
        this.refs.spinLoad.appendChild(this.spinner);
      }
      this.img.onload = this.onImgLoad;
      this.img.onerror = this.onImageError;
    }

    this.img.className = "hidden";
    this.img.src = source;

  }

  _isTallPortrait(w, h) {
    return w < h;
  }

  _onImgLoad() {

    if(this.spinner && this.refs.spinLoad && this.spinner.parentNode === this.refs.spinLoad) {
      this.spinner.parentNode.removeChild(this.spinner);
    }

    this.setState({
      portrait: this.isTallPortrait(this.img.width, this.img.height),
      src: this.props.src
    }, this.showImage);

    if(this.props.loaded)
      this.props.loaded();
  }

  _onImgLoadError() {
    this.setState({
      error: 1
    });
  }

  showImage() {
    if(typeof this.img !== 'undefined' && this.img) {
      this.img.className = 'fade';
      setTimeout(function() {
        this.img.className = (this.props.imgClass || 'thumb') + (this.state.portrait ? ' portrait' : '');
      }.bind(this), 100);
      
    } 
  }

  render() {

    if(this.state.error) {
      if(this.props.href)
        return <a href={this.props.href} className={this.props.class || ''}><i className="icon icon-image"/></a>;
      
      return false;
    }

    if(this.state.src) {
      var imgMarkup = <img alt={this.props.alt} className={'thumb' + (this.state.portrait ? ' portrait' : '')} src={this.state.src} />;
      
      if(this.props.href) {
        return <a href={this.props.href} className={this.props.class || ''}>{imgMarkup}</a>
      }
      return imgMarkup;
    }
    
    return (
      <span ref="spinLoad" className="img-wrap spin"></span>
    );

  }

}

window.SpinLoadImage = SpinLoadImage;