import React, {Component} from 'react';
import {Fetch} from '../../tools';
import {fire, addEvent, classParent, storageGet, storageSet, storageRemove, getVideoIndexByVideoId} from '../../tools/helpers';
import {VidPlayer} from '../VidPlayer';

import {VID_LIST_STORAGE_KEY, GOOGLE_API_KEY} from '../../config';

export class YoutubeSearch extends Component {

  static contextTypes = {}

  searchTimeout = 1
  state = {
    q: '',
    results: [],
    nextPageToken: "",
    loading: false,
    vidList: null,
  }

  componentDidMount() {
    this.setState({vidList: this.fetchVidList()});
    addEvent(window, 'vidListRefreshed', this.handleRefreshVidlist.bind(this));
  }

  handleRefreshVidlist(data) {
    this.setState({ vidList: data.detail.vidList });
  }

  handleOnChange = (e) => {
    this.setState({ q: e.target.value }, this.refreshSearch);
  }

  fetchResults = (reset = 1) => {

    if(this.state.q === "") {
      this.setState({
        results: [],
        nextPageToken: "",
      });
      return
      
    }

    if(this.state.q.length < 2)
      return;


    let fetch = new Fetch();
    fetch.request({
      service: "youtubeSearch",
      queryParams: {
        type: "video",
        part: "snippet",
        q: this.state.q,
        key: GOOGLE_API_KEY,
        pageToken: reset ? "" : this.state.nextPageToken,
      },
      success: (resp) => { 
        var results = reset ? resp.items : this.state.results.concat(resp.items);
        this.setState({
          results: results,
          nextPageToken: resp.nextPageToken || "",
          loading: false,
        }); 
      }
    });
  }

  refreshSearch() {
    if(this.searchTimeout)
      clearTimeout(this.searchTimeout);

    this.searchTimeout = setTimeout(this.fetchResults, 400)
  
  }

  results() {

    if(!this.state.results.length)
      return false;

  }

  handlePlayVideo = (e) => {
    e.preventDefault();
    var tgt = classParent(e.target, 'card-action', true);
    fire('showModal', {
      heading: tgt.dataset.title,
      markup: <div className="modal-menu"><VidPlayer vid={tgt.dataset.vid}/></div>, 
      close: true,
      wide: true,
      classes: 'dark tall centered bottom',
    });
  }

  fetchVidList() {
    const vidList = storageGet(VID_LIST_STORAGE_KEY);
    if('undefined' === typeof vidList || !vidList) {
      storageSet(VID_LIST_STORAGE_KEY, JSON.stringify([]))
      return [];
    }

    return JSON.parse(vidList);
  }

  resetVidlistStorage() {
    if(!this.state.vidList.length) {
      storageRemove(VID_LIST_STORAGE_KEY);
      return;
    }

    storageSet(VID_LIST_STORAGE_KEY, JSON.stringify(this.state.vidList));
  }

  toggleVidlistVideo = (e) => {
    e.preventDefault();

    var tgt = classParent(e.target, 'card-action', true),
        videoId = tgt.dataset.vid,
        index = parseInt(tgt.dataset.index, 10);

    const vidList = this.state.vidList;
    const results = this.state.results;

    var indexFound = getVideoIndexByVideoId(videoId, vidList);
    if(indexFound !== false) {
      vidList.splice(indexFound, 1);
    } else {
      vidList.push(results[index]);
      fire('videoVidlisted');
    }
    
    this.setState({ vidList: vidList}, this.resetVidlistStorage);
  }

  resultItem = (item, key) => {

    var heartIcon = getVideoIndexByVideoId(item.id.videoId, this.state.vidList) !== false ? " text-danger" : "-hollow";
    return (
      <div key={'item-video' + key} className="card">

        <div className="card-inner clear">

          <span className="card-image">
            <img alt={item.snippet.title} src={item.snippet.thumbnails.medium.url} />
          </span>

          <div className="card-title clear">{item.snippet.title}</div>

          <div className="card-actions">
            <a href="play" title="Play" onClick={this.handlePlayVideo} data-title={item.snippet.title} data-vid={item.id.videoId} className="card-action">
              <i className="icon icon-play icon-l"></i>
            </a>
            <a href="wishlist" title="Save to wishlist" onClick={this.toggleVidlistVideo} data-index={key} data-vid={item.id.videoId} className="card-action">
              <i className={"icon icon-heart" + heartIcon + " icon-l"}/>
            </a>
          </div>

        </div>
      </div>
    )

  }

  handleLoadMore = (e) => {
    e.preventDefault();
    this.setState( {loading: true}, () => { this.fetchResults(0) });
  }

  loadMoreButton() {
    if(!this.state.results.length)
      return false;
    

    var loadMoreText = this.state.loading ? <i className="icon icon-spinner spinner-fast"></i> : "Load more videos";
    return (
      <button href="#" className="btn load-more" onClick={this.handleLoadMore}>{loadMoreText}</button>
    );
  }

  render() {
    if(null === this.state.vidList)
      return false;

    return (
      <div className="search-wrap clearfix">
        <input className="search-input" autoFocus onChange={this.handleOnChange} value={this.state.q} placeholder="Search youtube music videos..."/>
        <div className="results-wrap">
          <div className="result-list">{this.state.results.map(this.resultItem)}</div>
          {this.loadMoreButton()}
        </div>
      </div>
    )
  }
}