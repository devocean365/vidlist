export const addEvent = (target, eventName, cb) => {
  if(!target)
    return;

  if (target.addEventListener) {
    target.addEventListener(eventName, cb, false);
  } else {
    target.attachEvent("on" + eventName, cb);
  }
}

export const killEvent = (target, eventName, cb) => {
  if(!target)
    return;
  
  if (target.removeEventListener) {
    target.removeEventListener(eventName, cb);
  } else {
    target.detachEvent("on" + eventName, cb);
  }
}

export const freezeBody = () => {
  document.documentElement.classList.add('freeze');
  document.body.style.height = window.innerHeight + 'px';
}

export const unfreezeBody = () => {
  document.documentElement.classList.remove('freeze');
  document.body.style.height = 'auto';
}

export const fire = (evtName, data) => {
  var evt = document.createEvent("CustomEvent");
  evt.initCustomEvent(evtName, true, true, data);
  window.dispatchEvent(evt);
}

export const classParent = (target, className, first) => {
  if(!contains(target.classList, className)) {
    if(!first && !target.parentNode)
      return false;

    return classParent(target.parentNode, className);

  } else {
    return target;

  }
}

export const contains = (a, obj) => {
  if(!a)
    return false;

  var i = a.length;
  while (i--)
    if (a[i] === obj)
      return true;
  
  return false;
}

export const isMobileView = (width) => {
  var compareWidth = width || 768;
  return window.innerWidth < compareWidth;
}

export const storageGet = (key, remove) => {
  if(!localStorage)
    return false;
  
  var item = localStorage.getItem(key);
  if(remove)
    storageRemove(key);

  return item;
}

export const storageSet = (key, value) => {
  if(!localStorage)
    return false;
  
  return localStorage.setItem(key, value);
}

export const storageRemove = (key, value) => {
  if(!localStorage)
    return false;
  
  return localStorage.removeItem(key, value);
}

export const getVideoIndexByVideoId = (videoId, vidList) => {

  for(let i=0; i < vidList.length; i+=1) {
    if(!vidList[i].id || vidList[i].id.videoId !== videoId)
      continue;

    return i;
  }

  return false;
}