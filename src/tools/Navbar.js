import React, {Component} from 'react';
import {fire} from './helpers';
import {VidList} from '../components/VidList';
import {addEvent} from './helpers';

export class Navbar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      linkActive: '',
      mobile: 0,
    }

    this.toggleMenu = this._toggleMenu.bind(this);

  }

  componentDidMount() {
    addEvent(window, 'videoVidlisted', () => {
      this.refs.menuToggle.classList.add('pump');
      setTimeout(() => {this.refs.menuToggle.classList.remove('pump')}, 300);
    })
  }

  hideModal(cb) {
    fire('hideModal');
  }


  getBrandLogo() {
    if(!this.props.brand || !this.props.brand.logo)
      return this.getBrandTitle();

    return <img alt={this.getBrandTitle()} className="logo logo-top" src={this.props.brand.logo} />;
  }

  getBrandTitle() {
    if(!this.props.brand || !this.props.brand.title)
      return false;


    return [this.props.brand.title, <span className="alpha">alpha</span>];
  }

  renderNavbarBrand() {
    var mItems = this.props.menu.items;
    if(!mItems || !mItems.length || !mItems[0].brand || !mItems[0].path)
      return (
        <a role="button" data-scroll className={"navbar-brand pull-left " + (this.props.brand.class ? this.props.brand.class : '')}>{this.getBrandLogo()}</a>
      );

    return (
      <a role="button" data-scroll className={"navbar-brand pull-left " + (this.props.brand.class ? this.props.brand.class : '')} href={mItems[0].path}>{this.getBrandLogo()}</a>
    );
  }

  renderModalMenu() {
    return (
      <ul className="modal-menu"><VidList /></ul>
    );
  }

  _toggleMenu(e) {
    e.preventDefault();
    var classes = 'tall right';
    fire('showModal', {heading: 'My vidlist', markup: this.renderModalMenu(), close: true, wide: 1, classes: classes});
  }

  renderMenuToggleButton() {
    
    if(this.props.menuOff)
      return false;

    return (
      <a href="toggle-menu" ref="menuToggle" onClick={this.toggleMenu} className="navbar-menu-toggle text-danger"><i className="icon icon-xl icon-heart-hollow"></i></a>
    );
  }

  renderMenuButton() {
    return (
      <ul className="navbar-header">
        {this.renderMenuToggleButton()}
        {this.renderNavbarBrand()}
      </ul>
    );
  }
  
  setLinkActive(id) {
    this.setState({
      linkActive: id
    });
  }

  render() {
    return (
      <div>
        <div className={"do-navbar_inner" + (!this.props.wide ? ' container' : '')}>
          {this.renderMenuButton()}
        </div>
      </div>
    );  
  }

}

window.Navbar = Navbar;