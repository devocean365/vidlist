const XML_HTTP_REQUEST = "XMLHttpRequest";
const API_PREFIX = "";

const SERVICES = {
	"youtubeSearch": {
		url: "https://www.googleapis.com/youtube/v3/search/",
		method: "GET",
		headerParams: [
			{name: "X-Requested-With", default: XML_HTTP_REQUEST}
		],
		queryParams: [
			{name: "part", default: null},
			{name: "q", default: null},
			{name: "key", default: null},
			{name: "type", default: null},
			{name: "pageToken", default: null},
		],
		contentType: "application/json; charset=utf-8",
		parse: function(payload) {
			return JSON.stringify(payload);
		}
	},
}

export class Fetch {
	
	logError(data, r) {

		if(data.nolog)
			return;

		var params = {
			nolog: true,
			rawResp: true,
			payload: {
				responseStatus: r.apiResponseStatus,
				method: data.method,
				serviceUrl: data.serviceUrl,
				service: data.service,
				payload: data.payload,
				urlParams: data.urlParams,
				queryParams: data.queryParams,
				headerParams: data.headerParams
			},
			service: "logError",
			success: function(resp) {
				if(parseInt(resp, 10) !== 1) {
					console.log("Error: Could not write to error log!");
				}
			}
		};
		this.request(params);

	}

	parseUrl(service) {
		if(!service) return false;

		var url = service.url;
		if(service.endPoint) {
			url += service.endPoint;
		} 
		return url;

	}

	createUrl(service, data) {

    var url = data.pathOverride ? data.pathOverride : this.parseUrl(service);

    if(!url) return false;

    // URL PARAMS
      if(service.urlParams && data.urlParams) {
        for(var up = 0; up < service.urlParams.length; up += 1) {
          var uParamKey = service.urlParams[up].name;
          var paramValue = data.urlParams[uParamKey] || service.urlParams[up].default;
          url = url.replace("{" + uParamKey + "}", paramValue);
        }
			}

			// QUERY PARAMS
			var query = [];
			if(!data.dynamicQueryParams) {
				if(service.queryParams && data.queryParams) {

	      	
	      	for(var i = 0; i < service.queryParams.length; i += 1){
	      		var qParamKey = service.queryParams[i].name;

	      		if(data.queryParams.hasOwnProperty(qParamKey)) {
	      			query.push(encodeURIComponent(qParamKey) + "=" + encodeURIComponent(data.queryParams[qParamKey]));
	      		}	
	      	}
	      	if(query.length) {
	      		url += "?" + query.join("&");
	      	}
				}
			} else {
				if(data.queryParams) {
					for(var prop in data.queryParams) {
						if(data.queryParams.hasOwnProperty(prop)) {
							query.push(encodeURIComponent(prop) + "=" + encodeURIComponent(data.queryParams[prop]));
						}
					}
					if(query.length) {
	      		url += "?" + query.join("&");
	      	}
				}

			}
  
    return API_PREFIX + url;
			
  }

	request(data) {
		var service = SERVICES[data.service];
		if(!service) return false;

		var r = new XMLHttpRequest();

		var serviceUrl = this.createUrl(service, data);

		if(data.urlPrefix) {
			serviceUrl = data.urlPrefix + serviceUrl;
		}
		
		r.open(service.method, serviceUrl, !data.sync);
		r.setRequestHeader("Content-Type", service.contentType);

		
		if(service.headerParams && service.headerParams.length) {
			var params = service.headerParams;
			for(var i = 0; i < params.length; i += 1) {
				if(!data.headerParams) data.headerParams = {};

				r.setRequestHeader(params[i].name, data.headerParams[params[i].name] || params[i].default);
			}
		}

		if(!data.sync) {
			r.onreadystatechange = function () {
				if (r.readyState !== 4) {
					return;
				}

				var status = r.status;

				if(!data.rawResp && r.responseText !== "") {
					r = JSON.parse(r.responseText);
				} else {
					r = r.responseText || r.statusText;
				}

				if(data.complete)
					data.complete(r);

				if(status !== 200) {
					if(data.error) data.error(r);

					this.logError(data, r);
					return; 
				} 
				if(data.success) {
					data.success(r);

					if(r.apiResponseStatus && r.apiResponseStatus !== "SUCCESS")
						console.log("200 - FAIL: " + r.apiResponseStatus);
					
				}

				
			}.bind(this);

			r.onerror = function() {
				if(data.error) data.error(r);
				
				this.logError(data, r);
			}.bind(this);

			try {
				r.send(service.parse(data.payload));
			} catch(e) {
				
			}
			

		}	else {
			r.send();

			if(!data.rawResp && r.responseText !== "") {
				return JSON.parse(r.responseText);
			}
			
			return r;
		}
		
	}

}