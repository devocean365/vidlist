import React, {Component} from 'react';

import {addEvent, killEvent, freezeBody, unfreezeBody} from './helpers';

export class Modal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      hidden: true,
      visible: false,
      heading: false,
      markup: false,
      data: {}
    }
    this.duration = 400;
    this.handleInitModal = this._handleInitModal.bind(this);
    this.hide = this._hide.bind(this);
    this.refresh = this._refresh.bind(this);
    this.doShow = this._doShow.bind(this);
    this.init = this._init.bind(this);
  }

  componentDidMount() {
    addEvent(window, 'showModal', this.handleInitModal);
    addEvent(window, 'hideModal', this.hide);
  }

  componentWillUnmount() {
    killEvent(window, 'init', this.handleInitModal);
  }

  _handleInitModal(data) {
    if(this.state.hidden || !this.state.visible) {
       this.init(data.detail);
      return;
    }
      
    this.refresh(function() {
      this.init(data.detail);
    }.bind(this));
  }

  _refresh(cb) {
    this.hide();

    setTimeout(() => cb(), this.duration);
  }

  _init(data) {
    var state = this.state;
    state.data = data;

    this.setState(state, this.doShow);

    if(data.callback)
      data.callback();

  }

  _doShow() {

    this.setState({
      hidden: false
    });

    setTimeout(function() {
      this.setState({
        visible: true
      });
    }.bind(this), 0);

    freezeBody()

  }

  _hide() {
    if(!this.state.visible)
      return;

    this.setState({
      visible: false
    });

    setTimeout(function() {
      this.setState({
        hidden: true
      });
    }.bind(this), this.duration);

    unfreezeBody();
  }

  renderCloseButton() {
    return !this.state.data.close || <a className="do-modal_close" onClick={() => this.hide()}><i className="icon icon-close icon-super-xxl"></i></a>;
  }

  renderModalMarkup() {
    return !this.state.data.markup || <div className="do-modal_body clear">{this.state.data.markup}</div>;
  }

  renderModalHeading() {
    if(!this.state.data.heading) 
      return false;

    return <div className="do-modal_heading clear">{this.state.data.heading}</div>;
  }
  
  render() {

    var visibleClass = this.state.visible ? ' visible' : '',
        modalMarkup = [],
        displayClass = this.state.hidden ? '' : ' in',
        modalInnerClass = this.state.data.thin ? ' thin' : '';

    if(!this.state.hidden) {
      modalMarkup.push(<div key="do-modal_overlay" className="do-modal_overlay"></div>);
      modalMarkup.push(
        <div key="do-modal_outter" id="do-modal_outter" className={"do-modal_outter clear do-modal_body" + (this.state.data.full ? ' full' : '') + (this.state.data.classes ? ' ' + this.state.data.classes : ' default')}>
          <div className={"do-modal_inner " + (!this.state.data.wide ? "container" : "do-modal_grid") + modalInnerClass}>
            {this.renderModalHeading()}
            {this.renderModalMarkup()}
            {this.renderCloseButton()}
          </div>
        </div>
      );
    }

    return (
      <div className={"do-modal" + visibleClass + displayClass}>
        {modalMarkup}
      </div>
    )    
  }

};