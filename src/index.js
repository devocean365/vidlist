import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

import App from './App';
import {Modal, Navbar} from './tools';
import registerServiceWorker from './registerServiceWorker';

import {Links} from './Links';

const APP_NAME = 'Vidlist';

ReactDOM.render(<Modal />, document.getElementById('modal'));
ReactDOM.render(<Navbar brand={{title: APP_NAME, logo: "/logo.png"}} wide={true} menu={{items: []}}/>, document.getElementById('navbar'));


ReactDOM.render(<App links={Links}/>, document.getElementById('root'));
registerServiceWorker();
